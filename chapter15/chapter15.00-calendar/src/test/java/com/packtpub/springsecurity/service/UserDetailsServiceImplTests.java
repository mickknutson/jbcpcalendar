package com.packtpub.springsecurity.service;

import com.packtpub.springsecurity.CalendarStubs;
import com.packtpub.springsecurity.repository.CalendarUserRepository;
import com.packtpub.springsecurity.web.controllers.EventsControllerTests;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@WebMvcTest(UserDetailsService.class)
public class UserDetailsServiceImplTests {

    @Autowired
    private UserDetailsService userDetailsService;

    @MockBean
    private CalendarUserRepository calendarUserRepository;

    @Before
    public void beforeEachMethod(){
        UserDetailsService uds = new UserDetailsServiceImpl();
    }

    /*
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {

        CalendarUser user = userRepository.findByEmail(username);

        if (user == null)
            throw new UsernameNotFoundException("username " + username
                    + " not found");

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (Role role : user.getRoles()){
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        }

        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), grantedAuthorities);
    }
     */
	@Test
	public void loadUserByUsername_null_user() {
        String username = "user1@example.com";

        org.springframework.security.core.userdetails.User user;

//        given(this.userDetailsService.loadUserByUsername(username))
//                .willReturn(CalendarStubs.user1());


        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        assertThat(userDetails.getUsername()).isEqualTo(username);
        assertThat(userDetails.getAuthorities().size()).isEqualTo(1);
    }

	@Test
	public void loadUserByUsername_user1() {
        String username = "user1@example.com";

        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        assertThat(userDetails.getUsername()).isEqualTo(username);
        assertThat(userDetails.getAuthorities().size()).isEqualTo(1);
    }

	@Test
	public void loadUserByUsername_admin1() {
        String username = "admin1@example.com";

        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        assertThat(userDetails.getUsername()).isEqualTo(username);
        assertThat(userDetails.getAuthorities().size()).isEqualTo(2);
    }

} // The End...
