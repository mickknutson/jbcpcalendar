package com.packtpub.springsecurity.web.controllers;

import com.gargoylesoftware.htmlunit.WebClient;
import com.packtpub.springsecurity.CalendarStubs;
import com.packtpub.springsecurity.domain.CalendarUser;
import com.packtpub.springsecurity.service.CalendarService;
import com.packtpub.springsecurity.service.UserContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 *
 * @author Mick Knutson
 *
 */
@DirtiesContext
@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc

public class EventsControllerTests {

    @Autowired
    private MockMvc mvc;


    @MockBean
    private CalendarService calendarService;

    @MockBean
    private UserContext userContext;

    //-----------------------------------------------------------------------//

    @Test
    public void chapter15_00__noops(){
    }

    //-----------------------------------------------------------------------//

    @Test
    @WithUserDetails("user1@example.com")
    public void test_User_MyEvents() throws Exception {
        given(this.userContext.getCurrentUser())
                .willReturn(CalendarStubs.user1());

        mvc
                .perform(get("/events/my"))
                .andExpect(status().isOk());
    }


    @Test
    @WithUserDetails("user1@example.com")
    public void test_User_getEvent() throws Exception {
        // FIXME: Find the anyString()
        given(this.calendarService.getEvent(102))
                .willReturn(CalendarStubs.toCreate());

        mvc
                .perform(get("/events/102"))
                .andExpect(status().isOk());
    }

    @Test
    @WithUserDetails("user1@example.com")
    public void test_User_Event_by_User() throws Exception {

        given(this.calendarService.getUser(0))
                .willReturn(CalendarStubs.user1());

        mvc
                .perform(get("/events/my?userId=0"))
                .andExpect(status().isOk());
    }

/*
    @Test
    @WithUserDetails("admin1@example.com")
    public void test_Admin_Event_by_User() throws Exception {

        given(this.calendarService.getUser(0))
                .willReturn(CalendarStubs.admin1());
        mvc
                .perform(get("/events/my?userId=1"))
                .andExpect(status().is5xxServerError())
                .andExpect(view().name("error"))
                .andExpect(content()
                        .string(
                                containsString("Exception during execution of Spring Security application!")
                        )
                ).andExpect(content()
                        .string(
                                containsString("Access is denied")
                        )
                );
    }
*/
/*

    @Test
    @WithUserDetails("user1@example.com")
    public void test_AllEvents_USER() throws Exception {
        mvc
                .perform(get("/events/"))
                .andExpect(status().isForbidden());
    }
*/

    @Test
    @WithUserDetails("admin1@example.com")
    public void test_AllEvents_ADMIN() throws Exception {
        mvc
                .perform(get("/events/"))
                .andExpect(status().isOk());
    }


} // The End...