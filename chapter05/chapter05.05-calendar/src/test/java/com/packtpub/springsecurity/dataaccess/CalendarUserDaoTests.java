package com.packtpub.springsecurity.dataaccess;

import com.packtpub.springsecurity.CalendarStubs;
import com.packtpub.springsecurity.domain.CalendarUser;
import com.packtpub.springsecurity.domain.Event;
import com.packtpub.springsecurity.domain.Role;
import com.packtpub.springsecurity.repository.CalendarUserRepository;
import com.packtpub.springsecurity.repository.RoleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DirtiesContext
@Transactional

@SpringBootTest
@AutoConfigureMockMvc
public class CalendarUserDaoTests {

    @Autowired
    private CalendarUserDao calendarUserDao;

    @Autowired
    private CalendarUserRepository calendarUserRepository;
    @Autowired
    private RoleRepository roleRepository;


    //------------- CALENDAR ------------------------------------------------//
    @Test(expected = IllegalArgumentException.class)
    public void chapter05_05__CalendarUserDao_constuctor_test_Null_CalendarUser_Repository() throws Exception {

        CalendarUserDao dao = new MongoCalendarUserDao(null,
                roleRepository);
    }

    @Test(expected = IllegalArgumentException.class)
    public void chapter05_05__constuctor_test_Null_RoleRepository_Repository() throws Exception {

        CalendarUserDao dao = new MongoCalendarUserDao(calendarUserRepository,
                null);
    }



    @Test(expected = IllegalArgumentException.class)
	public void test_findUsersByEmail_null_email() {
        calendarUserDao.findUsersByEmail(null);
    }

    @Test(expected = IllegalArgumentException.class)
	public void test_findUsersByEmail_blank_email() {
        calendarUserDao.findUsersByEmail("");
    }

    //-----------------------------------------------------------------------//
    @Test
    public void noop() throws Exception {}


    //-----------------------------------------------------------------------//
    @Test
    public void chapter05_05__getUser_Id_0() {
        CalendarUser user = calendarUserDao.getUser(0);
        assertThat(user.getId()).isEqualTo(0);
        assertThat(user.getRoles().size()).isEqualTo(1);
        assertThat(user.getFirstName()).as("check %s's First Name", user.getEmail()).isEqualTo("User");
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_null_Email() {
        calendarUserDao.findUserByEmail(null);
    }

    @Test
	public void validateUser_NotFound() {
        String email = "FOOBAR@example.com";
        CalendarUser user = calendarUserDao.findUserByEmail(email);
        assertThat(user).isNull();
	}

    @Test
	public void validateUser_User() {
        String email = "user1@example.com";
        CalendarUser user = calendarUserDao.findUserByEmail(email);
        assertThat(user.getEmail()).isEqualTo(email);
        assertThat(user.getRoles().size()).isEqualTo(1);
        assertThat(user.getFirstName()).as("check %s's First Name", user.getEmail()).isEqualTo("User");
	}

	@Test
	public void validateUser_Admin() {
	    String email = "admin1@example.com";
        CalendarUser user = calendarUserDao.findUserByEmail(email);
        assertThat(user.getEmail()).isEqualTo(email);
        assertThat(user.getRoles().size()).isEqualTo(2);
	}

	@Test
	public void createUser() {
        CalendarUser user = CalendarStubs.bob1();
//        user.setId(null);
        int userId = calendarUserDao.createUser(user);
        assertThat(userId).isGreaterThanOrEqualTo(2);
	}

    @Test(expected = IllegalArgumentException.class)
	public void createUser_null_User() {
        int userId = calendarUserDao.createUser(null);
        assertThat(userId).isEqualTo(42);
	}

	@Test
	public void test_findUsersByEmail() {
        String email = "admin1@example.com";
        List<CalendarUser> users = calendarUserDao.findUsersByEmail(email);
        assertThat(users.size()).isEqualTo(4);
    }


    //-----------------------------------------------------------------------//

    @Test
    public void test_Event_misc() {
        CalendarUser user = CalendarStubs.user1();

        assertThat(user.getLastName()).isNotBlank();
        assertThat(user.getName()).isNotBlank();
    }

    @Test
    public void test_Event_equals_null_object() {
        CalendarUser user = CalendarStubs.user1();

        boolean result = user.equals(null);
        assertThat(result).isFalse();
    }

    @Test
    public void test_Event_equals_self() {
        CalendarUser user = CalendarStubs.user1();

        boolean result = user.equals(user);
        assertThat(result).isTrue();
    }

    @Test
    public void test_Event_equals_same() {
        CalendarUser user = CalendarStubs.user1();
        CalendarUser user2 = CalendarStubs.user1();

        boolean result = user.equals(user2);
        assertThat(result).isTrue();
    }

    @Test
    public void test_Event_not_same_class() {
        CalendarUser user = CalendarStubs.user1();

        boolean result = user.equals(new Object());
        assertThat(result).isFalse();
    }


    @Test
    public void test_Event_equals_null_self_id() {
        CalendarUser user = CalendarStubs.user1();
        CalendarUser user2 = CalendarStubs.user2();
        user.setId(null);

        boolean result = user.equals(user2);
        assertThat(result).isFalse();
    }

    @Test
    public void test_Event_equals_null_other_id() {
        CalendarUser user = CalendarStubs.user1();
        CalendarUser user2 = CalendarStubs.user2();
        user2.setId(null);

        boolean result = user.equals(user2);
        assertThat(result).isFalse();
    }

    @Test
    public void test_Event_equals_null_both_ids() {
        CalendarUser user = CalendarStubs.user1();
        CalendarUser user2 = CalendarStubs.user2();
        user.setId(null);
        user2.setId(null);

        boolean result = user.equals(user2);
//        assertThat(result).isFalse();
    }



    //-----------------------------------------------------------------------//

} // The end...
