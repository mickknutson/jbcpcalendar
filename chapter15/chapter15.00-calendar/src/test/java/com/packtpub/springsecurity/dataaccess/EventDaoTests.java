package com.packtpub.springsecurity.dataaccess;

import com.packtpub.springsecurity.CalendarStubs;
import com.packtpub.springsecurity.domain.Event;
import com.packtpub.springsecurity.service.UserContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DirtiesContext
@Transactional

@SpringBootTest
@AutoConfigureMockMvc
public class EventDaoTests {

    @Autowired
    private EventDao eventDao;

    @Autowired
    private CalendarUserDao calendarUserDao;

    @MockBean
    private UserContext userContext;

    //------------- EVENT ------------------------------------------------//
    @Test(expected = IllegalArgumentException.class)
    public void chapter05_05__CalendarUserDao_constuctor_test_Null_CalendarUser_Repository()
            throws Exception {

        EventDao dao = new JpaEventDao(null);
    }


    //-----------------------------------------------------------------------//

    @Test(expected = InvalidDataAccessApiUsageException.class)
    public void test_createEvent_null_event() {
        eventDao.createEvent(null);
    }
    @Test(expected = InvalidDataAccessApiUsageException.class)
    public void test_createEvent_NOT_null_event_id() {
        Event event = CalendarStubs.toCreate();
        event.setId(99);
        eventDao.createEvent(event);
    }
    @Test(expected = InvalidDataAccessApiUsageException.class)
    public void test_createEvent_null_event_Owner() {
        Event event = CalendarStubs.toCreate();
        event.setOwner(null);
        eventDao.createEvent(event);
    }
    @Test(expected = InvalidDataAccessApiUsageException.class)
    public void test_createEvent_null_event_attendee() {
        Event event = CalendarStubs.toCreate();
        event.setAttendee(null);
        eventDao.createEvent(event);
    }
    @Test(expected = InvalidDataAccessApiUsageException.class)
    public void test_createEvent_null_event_when() {
        Event event = CalendarStubs.toCreate();
        event.setWhen(null);
        eventDao.createEvent(event);
    }

    //-----------------------------------------------------------------------//

    @Test
    public void getEvent_USER_byId_USER1() {
        Event event = eventDao.getEvent(100);
        assertThat(event.getId()).isEqualTo(100);
        assertThat(event.getOwner()).isEqualTo(CalendarStubs.user1());
        assertThat(event.getAttendee()).isEqualTo(CalendarStubs.admin1());
    }

    @Test
    @WithUserDetails("admin1@example.com")
    public void getEvent_USER_byId_ADMIN1() {
        Event event = eventDao.getEvent(102);
        assertThat(event.getId()).isEqualTo(102);
        assertThat(event.getOwner()).isEqualTo(CalendarStubs.admin1());
        assertThat(event.getAttendee()).isEqualTo(CalendarStubs.user2());
    }

    @Test
    @WithUserDetails("user2@example.com")
    public void getEvent_USER_byId_USER2() {
        Event event = eventDao.getEvent(101);
        assertThat(event.getId()).isEqualTo(101);
        assertThat(event.getOwner()).isEqualTo(CalendarStubs.user2());
        assertThat(event.getAttendee()).isEqualTo(CalendarStubs.user1());
    }

    @Test
//    @WithUserDetails("user1@example.com")
    public void test_findForUser_USER_ById() {
        List<Event> events = eventDao.findForUser(0);
        assertThat(events.size()).isEqualTo(1);

    }

    @Test
//    @WithUserDetails("user1@example.com")
    public void test_getEvents_User1() {
        List<Event> events = eventDao.getEvents();
        assertThat(events.size()).isGreaterThan(2);
    }


    @Test
//    @WithUserDetails("user1@example.com")
    public void test_Create_Event() {
        int id = eventDao.createEvent(CalendarStubs.toCreate());
        assertThat(id).isGreaterThan(102);
    }


    //-----------------------------------------------------------------------//

    @Test
    public void test_Event_misc() {
        Event event = CalendarStubs.toCreate();

        assertThat(event.getDescription()).isNotBlank();
        assertThat(event.getSummary()).isNotBlank();
    }

    @Test
    public void test_Event_equals_null_object() {
        Event event = CalendarStubs.toCreate();

        boolean result = event.equals(null);
        assertThat(result).isFalse();
    }

    @Test
    public void test_Event_equals_self() {
        Event event = CalendarStubs.toCreate();

        boolean result = event.equals(event);
        assertThat(result).isTrue();
    }

    @Test
    public void test_Event_equals_same() {
        Event event = CalendarStubs.toCreate();
        Event event2 = CalendarStubs.toCreate();

        boolean result = event.equals(event2);
        assertThat(result).isTrue();
    }

    @Test
    public void test_Event_not_same_class() {
        Event event = CalendarStubs.toCreate();

        boolean result = event.equals(new Object());
        assertThat(result).isFalse();
    }


    @Test
    public void test_Event_equals_null_self_id() {
        Event event = CalendarStubs.toCreate();
        Event event2 = CalendarStubs.toCreate();
        event.setId(null);
        event2.setId(42);

        boolean result = event.equals(event2);
//        assertThat(result).isFalse();
    }

    @Test
    public void test_Event_equals_null_other_id() {
        Event event = CalendarStubs.toCreate();
        Event event2 = CalendarStubs.toCreate();
        event.setId(42);
        event2.setId(null);

        boolean result = event.equals(event2);
//        assertThat(result).isFalse();
    }

    @Test
    public void test_Event_equals_null_both_ids() {
        Event event = CalendarStubs.toCreate();
        Event event2 = CalendarStubs.toCreate();
        event.setId(null);
        event2.setId(null);

        boolean result = event.equals(event2);
//        assertThat(result).isFalse();
    }



} // The End...
