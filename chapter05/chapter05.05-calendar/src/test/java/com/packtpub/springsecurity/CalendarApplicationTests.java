package com.packtpub.springsecurity;

import com.packtpub.springsecurity.dataaccess.CalendarUserDao;
import com.packtpub.springsecurity.dataaccess.EventDao;
import com.packtpub.springsecurity.dataaccess.MongoCalendarUserDao;
import com.packtpub.springsecurity.dataaccess.MongoEventDao;
import com.packtpub.springsecurity.repository.CalendarUserRepository;
import com.packtpub.springsecurity.repository.EventRepository;
import com.packtpub.springsecurity.repository.RoleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext
@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest

@AutoConfigureMockMvc
public class CalendarApplicationTests {

    @Autowired
    MockMvc mvc;

    @Autowired
    private CalendarUserDao calendarUserDao;
    @Autowired
    private CalendarUserRepository calendarUserRepository;

    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private RoleRepository roleRepository;


    //-----------------------------------------------------------------------//
    @Test
    public void noop() throws Exception {}


    //----------------
    @Test
    public void chapter05_05__user1_Login() throws Exception {
        mvc.perform(post("/login")
                        .accept(MediaType.TEXT_HTML)
                        .contentType(
                                MediaType.APPLICATION_FORM_URLENCODED)
                        .param("username", "user1@example.com")
                        .param("password", "user1")
        )
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/default"))
                .andDo(print())
        ;
    }

} // The End...
