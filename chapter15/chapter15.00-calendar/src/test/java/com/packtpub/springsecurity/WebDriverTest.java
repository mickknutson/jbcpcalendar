package com.packtpub.springsecurity;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.*;

import static org.junit.Assert.*;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.runner.RunWith;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * http://toolsqa.com/selenium-webdriver/how-to-use-geckodriver/
 * https://sites.google.com/a/chromium.org/chromedriver/home
 *
 * http://www.automationtestinghub.com/selenium-3-0-launch-firefox-with-geckodriver/
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

//@AutoConfigureMockMvc
public class WebDriverTest {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @LocalServerPort
    private int port;

    private WebDriver driver;
    private static String baseDir = "./build/reports/";
    private String baseHost = "localhost";
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuilder verificationErrors = new StringBuilder();

    @Before
    public void beforEachTest()
            throws Exception {

        this.baseUrl = "http://" + baseHost +":"+ port;
        logger.info("//--------------------------------------------//");
        logger.info("port: {}", port);
        logger.info("baseUrl: {}", baseUrl);

//        driver = SeleniumTestUtilities.getChromeDriver();
//        driver = SeleniumTestUtilities.getFirefoxDriver();
        driver = SeleniumTestUtilities.getHtmlUnitDriver();
//        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

    @After
    public void afterEachTest() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    //-----------------------------------------------------------------------//

    @Test
    public void chapter15_00__noop() throws Exception {}

    @Test
    public void test__WebDriver_events_user1() throws Exception {
        String target = "/events";

        driver.get(baseUrl + target);
//        this.captureScreenshot(driver, target+"_user1") ;

        driver.findElement(By.name("username")).sendKeys("user1@example.com");
        driver.findElement(By.name("password")).sendKeys("user1");
        driver.findElement(By.name("submit")).click();

        assertThat(driver.getTitle()).isEqualToIgnoringCase("Welcome to myCalendar!");
//        this.captureScreenshot(driver, target+"_user1_loggedin") ;


        assertThat(this.isElementPresent(By.id("navMyEventsLink"))).isTrue();
        driver.findElement(By.id("navMyEventsLink")).click();
//        WebElement element = driver.findElement(By.id("navMyEventsLink")).click();
//        element.click();
//        assertThat(element.getText()).isEqualToIgnoringCase("My Events");
        assertThat(driver.getTitle()).isEqualToIgnoringCase("My Events");

        // https://localhost:8443/events/my
//        this.captureScreenshot(driver, target+"_user1_events") ;
        assertThat(driver.getPageSource()).containsIgnoringCase("Birthday Party");
    }

//    @Test
    public void test__WebDriver_events_admin1() throws Exception {
        String target = "/events";
        logger.info("//--------------------------------------------//");
        logger.info("target: {}", target);

        driver.get(baseUrl + target);
//        driver.wait(10_000L);
        this.captureScreenshot(driver, target+"_admin1") ;
        this.takeSnapShot(driver, target+"_admin1") ;

        driver.findElement(By.name("username")).sendKeys("admin1@example.com");
        driver.findElement(By.name("password")).sendKeys("admin1a");
        driver.findElement(By.name("submit")).click();

        assertThat(driver.getTitle()).isEqualToIgnoringCase("All Events");
        this.captureScreenshot(driver, target+"_admin1_loggedin") ;

        assertThat(this.isElementPresent(By.id("navEventsLink"))).isTrue();
        assertThat(this.isElementPresent(By.id("navMyEventsLink"))).isTrue();

        this.captureScreenshot(driver, target+"_admin1_events") ;
        assertThat(driver.getPageSource()).containsIgnoringCase("Birthday Party");
    }


    //-----------------------------------------------------------------------//

    public static String captureScreenshot(final WebDriver driver,
                                           final String screenshotName)
    throws IOException{

        try {
            TakesScreenshot ts = (TakesScreenshot)driver;
            File source = ts.getScreenshotAs(OutputType.FILE);
            String dest = baseDir + screenshotName + ".png";
            File destination = new File(dest);
            FileUtils.copyFile(source, destination);
            return dest;
        }
        catch (IOException e) {return e.getMessage();}
    }

    //-----------------------------------------------------------------------//

    public static void takeSnapShot(WebDriver webdriver,
                                    String fileWithPath)
            throws Exception{

        // Convert web driver object to TakeScreenshot
        TakesScreenshot scrShot = ((TakesScreenshot) webdriver);

        // Call getScreenshotAs method to create image file
        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);

        // Move image file to new destination
        File DestFile=new File(baseDir + fileWithPath);

        // Copy file at destination
        FileUtils.copyFile(SrcFile, DestFile);

    }
    //-----------------------------------------------------------------------//

    //-----------------------------------------------------------------------//

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
    //-----------------------------------------------------------------------//


} // The End...
