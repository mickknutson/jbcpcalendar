package com.packtpub.springsecurity.web.controllers;

import com.packtpub.springsecurity.authentication.CalendarUserAuthenticationProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 *
 * @author Mick Knutson
 *
 */
@DirtiesContext
@Transactional

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class WelcomeControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private Authentication authentication;

    @Autowired
    private CalendarUserAuthenticationProvider authenticationProvider;


    //-----------------------------------------------------------------------//

/*
@ModelAttribute("showCreateLink")
    public boolean showCreateLink(Authentication authentication) {
        // NOTE We could also get the Authentication from SecurityContextHolder.getContext().getAuthentication()
        return authentication != null && authentication.getName().contains("user");
    }

        @Test
    public void getVehicleShouldReturnMakeAndModel() {
        given(this.userVehicleService.getVehicleDetails("sboot"))
            .willReturn(new VehicleDetails("Honda", "Civic"));

        this.mvc.perform(get("/sboot/vehicle")
            .accept(MediaType.TEXT_PLAIN))
            .andExpect(status().isOk())
            .andExpect(content().string("Honda Civic"));
    }

 */
/*

    @Test
    public void test_showCreateLink_null_authentication() throws Exception {

        mvc
                .perform(get("/showCreateLink"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("index"))
                .andDo(print())
        ;
    }

    @Test
    @WithUserDetails("user1@example.com")
    public void test_showCreateLink() throws Exception {
        given(this.authentication.getName().contains("user"))
                .willReturn(true);

        mvc
                .perform(get("/showCreateLink"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andDo(print())
        ;
    }


    @Test
    @WithUserDetails("admin1@example.com")
    public void test_admin1_showCreateLink() throws Exception {
//        mvc
//                .perform(get("/showCreateLink"))
//                .andExpect(status().is3xxRedirection())
//                .andExpect(redirectedUrl("/events/"))
//                .andDo(print())
//        ;
    }
*/


    @Test
    @WithAnonymousUser
    public void test_welcome_WithAnonymousUser() throws Exception {
        mvc
                .perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andDo(print())
        ;
    }

    @Test
    public void test_welcome() throws Exception {
        mvc
                .perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andDo(print())
        ;
    }




    //-----------------------------------------------------------------------//

    private Authentication userAuth(){
        String username = "user1@example.com";
        String password = "user1";

        UsernamePasswordAuthenticationToken authRequest =
                new UsernamePasswordAuthenticationToken(username, password);
        return authenticationProvider.authenticate(authRequest);
    }

    private Authentication adminAuth(){
        String username = "admin@example.com";
        String password = "admin";

        UsernamePasswordAuthenticationToken authRequest =
                new UsernamePasswordAuthenticationToken(username, password);
        return authenticationProvider.authenticate(authRequest);
    }


} // The End...